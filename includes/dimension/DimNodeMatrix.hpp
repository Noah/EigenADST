#pragma once
#include "DimNode.hpp"

#include <string>
#include <memory>

#include "Dimension.hpp"
#include "DimensionType.hpp"

class Dimension;

/**
 * @class DimNodeMatrix
 * @brief Class representing a matrix node in the dimension graph. This class is a subclass of DimNode.
 * @details A DimNodeMatrix represents one of the two resulting dimensions (row/column) of a ExpMatrix object.
 */
class DimNodeMatrix : public DimNode {
public:
    /**
     * @brief Constructor for DimNodeMatrix.
     * @param hashID The hash ID of the matrix this node represents a dimension of.
     * @param name The name of the matrix this node represents a dimension of.
     * @param dimType The dimension type of matrix this node represents a dimension of.
     * @param dim The shared pointer to the dimension object it refers to.
     */
	DimNodeMatrix(const std::string& hashID, const std::string& name,
		DimensionType dimType, std::shared_ptr<Dimension> dim);
    /**
     * @brief Overridden helper function that sets the dimension of the node.
     * @details A dimension is only set if the DimNode is of type DimNodeMatrix because a DimNode does not have a dimension pointer.
     * @param dim The dimension to set.
     */
	void setDimension(const Dimension& dim) override;
protected:
    /**
     * @brief The shared pointer to the dimension object it refers to.
     * @details This shared pointer points to one of the Dimension objects in a ExpMatrix object and is later used to set the size of the Dimension object of the said ExpMatrix object.
     */
	std::shared_ptr<Dimension> dim_;
};