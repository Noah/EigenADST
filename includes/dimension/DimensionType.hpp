#pragma once
#include <string>

/**
 * @enum DimensionType
 * @brief Enumeration representing the type of a dimension.
 */
enum class DimensionType {
	Row,
	Column,
	Unset,
};

/**
 * @brief Convert a DimensionType to its string representation.
 * @param type The DimensionType to convert.
 * @return The string representation of the DimensionType.
 */
static std::string DimTypeToString(DimensionType type) {
	switch (type) {
	case DimensionType::Row:
		return "Row";
	case DimensionType::Column:
		return "Column";
	case DimensionType::Unset:
		return "Unset";
	default:
		return "Unknown";
	}
}
