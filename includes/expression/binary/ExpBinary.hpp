#pragma once
#include "expression/Expression.hpp"
#include <string>
#include <memory>
#include <vector>

#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"
class ExpMatrix;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpBinary
 * @brief Abstract super class for binary expressions. This class is derived from the Expression base class.
 */
class ExpBinary : public Expression {
public:
    ExpBinary() = default;
    /**
     * @brief Constructs a binary expression with the given left and right expressions.
     * @details This constructor is used when first creating the tree and all other member variables are unknown.
     * @param left A shared pointer to the left-hand side expression.
     * @param right A shared pointer to the right-hand side expression.
     */
    ExpBinary(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right);
    /**
     * @brief Constructs a binary expression with all given member variables.
     * @details This constructor is used when a deepCopy is needed.
     * @param left A shared pointer to the left-hand side expression.
     * @param right A shared pointer to the right-hand side expression.
     * @param resultType The result type of the assignment expression.
     */
    ExpBinary(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right, ExpressionResultType resultType);

    virtual std::string toString() const = 0;
    virtual std::shared_ptr<Expression> tangDifferentiate() const = 0;
    virtual void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const = 0;
    virtual std::shared_ptr<Expression> deepCopy() const = 0;
    virtual ExpressionResultType checkSemantics() = 0;
    virtual void setDimensionNodes(DimGraph& graph) = 0; 
    virtual std::variant<ScalarValueType, MatrixType> calculate() const = 0;
    virtual void generateMatrixValues(MatrixCache& matrixValuesCache) override; //overridden by ExpCholesk
    virtual void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache); //overridden by ExpCholesk
    virtual std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const = 0;
    virtual ExpressionType getExpressionType() const = 0;
    virtual void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) = 0;
    std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const = 0;

protected:
    /**
     * @brief A shared pointer to the left-hand side Expression.
     */
    std::shared_ptr<Expression> left_;
    /**
     * @brief A shared pointer to the right-hand side Expression
     */
    std::shared_ptr<Expression> right_;

};