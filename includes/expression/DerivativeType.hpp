#pragma once

/**
 * @enum DerivativeType
 * @brief This enum represents the possible types of derivatives.
 */
enum class DerivativeType {
	Primal,
	Adjoint,
	Tangent,
};

/**
 * @brief Converts a given DerivativeType to its corresponding string representation.
 * @param type: The DerivativeType to convert.
 * @return A string representation of the DerivativeType. Returns "?" if the DerivativeType is not recognized. 
 */
static std::string DerivTypeToString(DerivativeType type) {
	switch (type) {
	case DerivativeType::Primal:
		return "p";
	case DerivativeType::Adjoint:
		return "a";
	case DerivativeType::Tangent:
		return "t";
	default:
		return "?";
	}
}