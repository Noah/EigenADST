#pragma once
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/DerivativeType.hpp"

class ExpMatrix;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpTerminal
 * @brief Abstract super class for terminal expressions. This class is derived from the Expression base class.
 */
class ExpTerminal : public Expression {
public:
    virtual std::string toString() const = 0;
    virtual std::shared_ptr<Expression> tangDifferentiate() const = 0;
    virtual void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const = 0;
    virtual std::shared_ptr<Expression> deepCopy() const = 0;
    virtual ExpressionResultType checkSemantics() = 0;
    virtual std::variant<ScalarValueType, MatrixType> calculate() const = 0;
    virtual void setDimensionNodes(DimGraph& graph) = 0;
    virtual void generateMatrixValues(MatrixCache& matrixValuesCache) = 0;
    virtual std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const = 0;
    virtual ExpressionType getExpressionType() const = 0;
    virtual void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) = 0;
    void devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache);
    std::shared_ptr<Expression> transformExpCholeskToExpLowUpp() const override;
private:
};