#pragma once
#include "ExpTerminal.hpp"

#include <memory>
#include <string>
#include <vector>

#include "expression/ExpressionResultType.hpp"

#include "EigenNS.hpp"
#include "expression/DerivativeType.hpp"
#include "dimension/Dimension.hpp"

class Expression;
class DimNode; 

class DimGraph;
class ExpAssign;
class MatrixCache;

/**
 * @class ExpMatrix
 * @brief This class represents a matrix. This class is derived from the Expression base class.
 */
class ExpMatrix : public ExpTerminal{

public:
    /**
    * @brief Constructs a matrix with the given type and value.
    * @details This constructor is used when first creating the tree and all other member variables are unknown.
    * @param name The name of this matrix.
    * @param suffixes The suffixes of this matrix.
    * @param isAliases Flag if the variable this matrix represents is aliased.
    */
    ExpMatrix(std::string name, std::string suffixes, bool isAliased);
    /**
    * @brief Constructs a matrix with the given type and value.
    * @param name The name of this matrix.
    * @param suffixes The suffixes of this matrix.
    * @param mode The derivative type.
    * @param iteration The current iteration.
    * @param isAliases Flag if the variable this matrix represents is aliased.
    */
    ExpMatrix(std::string name, std::string suffixes, DerivativeType mode, int iteration, bool isAliased);
    /**
    * @brief Constructs a matrix with the given type and value.
    * @details This constructor is used when a deepCopy is needed.
    * @param name The name of this matrix.
    * @param suffixes The suffixes of this matrix.
    * @param mode The derivative type.
    * @param iteration The current iteration.
    * @param isAliases Flag if the variable this matrix represents is aliased.
    * @param isDiff The isDiff_ value.
    * @param resultType The result type of the matrix.
    * @param row A shared pointer to a Dimension object. 
    * @param column A shared pointer to a Dimension object.
    * @param value The values of the matrix.
    */
    ExpMatrix(std::string name, std::string suffixes, DerivativeType mode, int iteration, bool isAliased, bool isDiff, ExpressionResultType resultType,
        std::shared_ptr<Dimension> row, std::shared_ptr<Dimension> column, MatrixType value);
    std::string toString() const override;
    std::shared_ptr<Expression> tangDifferentiate() const override;
    void adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const override;
    void setDimensionNodes(DimGraph& graph) override;
    void perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) override;
    std::variant<ScalarValueType, MatrixType> calculate() const override;
    std::shared_ptr<Expression> deepCopy() const override;
    /**
     * @brief A helper function that deep copies a matrix.
     * @returns A shared pointer to the copied matrix.
     */
    std::shared_ptr<ExpMatrix> deepCopyMatrix() const;
    ExpressionResultType checkSemantics() override;
    std::string getHashID() const override;
    void generateMatrixValues(MatrixCache& matrixValuesCache) override;

    /**
     * @brief Returns isDiff_.
     * @returns True or false if the current matrix was already differentiated by this tool.
     */
    bool isDiff() const;
    /**
     * @brief Get the current name of this matrix.
     * @details Returns only the name_ variable.
     * @returns The name_ of this matrix.
     */
    std::string getName() const;
    /**
     * @brief Get the existing suffixes of this matrix.
     * @details Returns only the suffixes_ variable.
     * @returns The suffixes_ of this matrix.
     */
    std::string getSuffixes() const;
    /**
     * @brief Get the current mode of this matrix.
     * @returns The mode_ of this matrix.
     */
    DerivativeType getMode() const;
    /**
     * @brief Get the current iteration of this matrix.
     * @returns The iteration_ of this matrix.
     */
    int getIteration() const;

    /**
     * @brief Get the current Dimension object that describes the row of this matrix.
     * @returns The row_ of this matrix.
     */
    std::shared_ptr<Dimension> getRowDim();
    /**
     * @brief Get the current Dimension object that describes the column of this matrix.
     * @returns The column_ of this matrix.
     */
    std::shared_ptr<Dimension> getColDim();

    /**
     * @brief Get the current values of this matrix.
     * @returns The matrixValues_ of this matrix.
     */
    MatrixType getMatrixValues() const;
    /**
     * @brief Set the current values of this matrix.
     * @param matrixValues The matrix values to be set.
     */
    void setMatrixValues(MatrixType matrixValues);

    /**
     * @brief Initialises the matrix to represent an adjoint derivative of the original matrix.
     * @details This function is only called in the ExpAssign class to initialise the left-hand matrix. It changes the mode_ and the iteration_.
     */
    void initialiseAdjointMatrix();
    /**
     * @brief Initialises the matrix to represent a tangent derivative of the original matrix.
     * @details This function is only called in the ExpAssign class to initialise the left-hand matrix. It changes the mode_ and the iteration_.
     */
    void initialiseTangentMatrix();

    /**
     * @brief Gets the isAliased_ flag.
     * @returns The variable isAliased.
     */
    bool getIsAliased() const;

    /**
     * @brief Create a row DimNode for this expression, overrides its inherited method.
     * @details The resulting DimNodeMatrix is based upon the hash id and name of this ExpMatrix object.
     * The DimNodeMatrix object contains a pointer to the row_ Dimension object of this matrix. This override is specific to ExpMatrix objects to incorporate these additional details.
     * @returns A shared pointer to the created row DimNodeMatrix.
     */
    std::shared_ptr<DimNode> createRowDimNode() const override;
    /**
     * @brief Create a row DimNode for this expression, overrides its inherited method.
     * @details The resulting DimNodeMatrix is based upon the hash id and name of this ExpMatrix object.
     * The DimNodeMatrix object contains a pointer to the column_ Dimension object of this matrix. This override is specific to ExpMatrix objects to incorporate these additional details.
     * @returns A shared pointer to the created column DimNodeMatrix.
     */
    std::shared_ptr<DimNode> createColDimNode() const override;

    std::vector<std::vector<ExpressionResultType>> getRequiredTypes() const override;
    ExpressionType getExpressionType() const override;

private:
    /**
     * @brief Compares two matrices.
     * @details This function does not compare values, only the values relevant for the output.
     * @returns True or false whether both matrices represent the same matrix.
     */
    bool isEqualTo(const std::shared_ptr<ExpMatrix> mat);
    /**
     * @brief The values of the matrix.
     */
    MatrixType matrixValues_;
    /**
     * @brief A shared pointer to a Dimension object that describes the row of the matrix.
     * @details After calculating all Dimensions, a Dimension object does not need to be unique per matrix.
     */
    std::shared_ptr<Dimension> row_ = std::make_shared<Dimension>();
    /**
     * @brief A shared pointer to a Dimension object that describes the column of the matrix.
     * @details After calculating all Dimensions, a Dimension object does not need to be unique per matrix.
     */
    std::shared_ptr<Dimension> column_ = std::make_shared<Dimension>();

    /**
     * @brief The name of the matrix without any additional suffixes.
     * @details The name is part of the final output and extracted in the step of building the original expression.
     */
    std::string name_;
    /**
     * @brief The suffixes of the matrix (e.g. created by this tool in earlier iterations).
     */
    std::string suffixes_;
    /**
    * @brief The current mode of the matrix.
    * @details The mode is part of the final output and extracted in the step of building the original expression.
    */
    DerivativeType mode_ = DerivativeType::Primal;
    /**
     * @brief The current iteration of the matrix.
     * @details The iteration is part of the final output and extracted in the step of building the original expression.
     */
    int iteration_ = 0;
    /**
     * @brief True or False whether this matrix was differentiated by this tool.
     */
    bool isDiff_ = false;

    /**
     * @brief Flag if the variable that this matrix represents was present in the input and in the output of the initial statement.
     */
    bool isAliased_ = false;

};
