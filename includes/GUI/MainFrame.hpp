#pragma once
#include <wx/frame.h>
#include <wx/panel.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include "GUI/innerPanels/DeclarationsPanel.hpp"
#include "GUI/innerPanels/AdjointsPanel.hpp"
#include "GUI/innerPanels/TangentPanel.hpp"
#include "GUI/innerPanels/ConditionControlPanel.hpp"
#include "GUI/ButtonID.hpp"

#include <string>
#include <memory>
#include "expression/Expression.hpp"
#include "expression/FDAType.hpp"

/**
 * @class MainFrame
 * @brief Class that has the main window of the application, here all functionalities such as input and output come together.
 */
class MainFrame : public wxFrame {
public:
    /**
     * @brief Constructs the main window.
     * @param title The window title
     */
    MainFrame(const wxString& title);

    /**
     * @brief Handles the event of the start button being pressed.
     * @details This function runs the main execution flow of the program, which involves parsing an Eigen matrix expression,
     * performing semantic checking, generating matrix dimensions and random values, differentiating the expression (adjoints and tangent),
     * validating derivatives, and printing the original, the differentiated expressions and the matrix declarations to a textbox.
     * @param event The event object
     */
    void OnStartButtonPressed(wxCommandEvent& event);

    /**
     * @brief Handles the event of the copy declarations button being clicked. Copies the content of declarations output box to the clipboard.
     * @param event The event object
     */
    void OnCopyDeclButtonClicked(wxCommandEvent& event);

    /**
     * @brief Handles the event of the copy adjoints button being clicked. Copies the content of adjoints output box to the clipboard.
     * @param event The event object
     */
    void OnCopyAdjButtonClicked(wxCommandEvent& event);

    /**
     * @brief Handles the event of the copy tangents button being clicked. Copies the content of tangents output box to the clipboard.
     * @param event The event object
     */
    void OnCopyTangButtonClicked(wxCommandEvent& event);

    /**
     * @brief Handles the event of the quit menu item being selected and quits this program.
     * @param event The event object
     */
    void OnQuit(wxCommandEvent& event);

    /**
     * @brief Handles the event of the documentation menu item being selected. Opens the documentation page in the default browser.
     * @param event The event object
     */
    void OnDocumentation(wxCommandEvent& event);

    /**
     * @brief Handles the event of the about menu item being selected. Shows an about dialog.
     * @param event The event object
     */
    void OnAbout(wxCommandEvent& event);

private:
    /**
     * @brief Compares if two Eigen matrices are nearly equal.
     * @details The difference between the matrices can be bigger if both matrix values are also bigger. So the difference can be dynamic.
     * @param type The type of the FDA.
     * @param A Matrix A.
     * @param B Matrix B.
     */
    void validateMatrices(FDAType type, MatrixType A, MatrixType B) const;

    /**
     * @brief Parses the input assignments and returns a vector of expressions.
     * @param input The input string to parse
     * @param inputDerivativeOrder The order of derivative
     * @return A vector of expressions
     */
    std::vector<std::shared_ptr<ExpAssign>> parseInputAssignments(const std::string& input, int inputDerivativeOrder);

    /**
     * @brief Differentiates and validates the given expression.
     * @param exp The expression to differentiate and validate
     */
    void differentiateAndValidate(const std::shared_ptr<ExpAssign>& exp);

    /**
     * @brief A description of the input assignments box.
     */
    wxStaticText* inputAssignDescr_;

    /**
     * @brief A description of the derivative order input field.
     */
    wxStaticText* inputOrderDescr_;

    /**
     * @brief A text box for inputting assignments.
     */
    wxTextCtrl* inputAssignmentBox_;

    /**
     * @brief A panel for inputting the derivative order and the start button.
     */
    ConditionControlPanel* condContrPan_;

    /**
     * @brief A description of the matrix declarations' output field.
     */
    wxTextCtrl* declarationsOutBox_;

    /**
     * @brief A description of the adjoints' output field.
     */
    wxTextCtrl* adjointsOutBox_;

    /**
     * @brief A description of the tangents output field.
     */
    wxTextCtrl* tangentOutBox_;

    /**
     * @brief A panel for displaying matrix declarations and providing a copy functionality.
     */
    DeclarationsPanel* declPanel_;

    /**
     * @brief A panel for displaying adjoints and providing a copy functionality.
     */
    AdjointsPanel* adjPanel_;

    /**
     * @brief A panel for displaying the tangent and providing a copy functionality.
     */
    TangentPanel* tangPanel_;

    wxDECLARE_EVENT_TABLE();
};
