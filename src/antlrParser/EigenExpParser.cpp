
// Generated from EigenExp.g4 by ANTLR 4.12.0


#include "EigenExpVisitor.h"

#include "EigenExpParser.h"


using namespace antlrcpp;

using namespace antlr4;

namespace {

struct EigenExpParserStaticData final {
  EigenExpParserStaticData(std::vector<std::string> ruleNames,
                        std::vector<std::string> literalNames,
                        std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  EigenExpParserStaticData(const EigenExpParserStaticData&) = delete;
  EigenExpParserStaticData(EigenExpParserStaticData&&) = delete;
  EigenExpParserStaticData& operator=(const EigenExpParserStaticData&) = delete;
  EigenExpParserStaticData& operator=(EigenExpParserStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag eigenexpParserOnceFlag;
EigenExpParserStaticData *eigenexpParserStaticData = nullptr;

void eigenexpParserInitialize() {
  assert(eigenexpParserStaticData == nullptr);
  auto staticData = std::make_unique<EigenExpParserStaticData>(
    std::vector<std::string>{
      "scalarValue", "matrix", "assigns", "assignment", "expression"
    },
    std::vector<std::string>{
      "", "';'", "'='", "'.transpose()'", "'.llt().solve('", "')'", "'*'", 
      "'-'", "'+'", "'('"
    },
    std::vector<std::string>{
      "", "", "", "", "", "", "", "", "", "", "MATRIX_ID", "NUMBER", "WS"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,1,12,67,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,1,0,1,0,1,1,1,1,1,2,
  	1,2,5,2,17,8,2,10,2,12,2,20,9,2,5,2,22,8,2,10,2,12,2,25,9,2,1,2,1,2,1,
  	3,1,3,1,3,1,3,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,3,4,44,8,4,
  	1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,1,4,5,4,62,
  	8,4,10,4,12,4,65,9,4,1,4,0,1,8,5,0,2,4,6,8,0,0,72,0,10,1,0,0,0,2,12,1,
  	0,0,0,4,23,1,0,0,0,6,28,1,0,0,0,8,43,1,0,0,0,10,11,5,11,0,0,11,1,1,0,
  	0,0,12,13,5,10,0,0,13,3,1,0,0,0,14,18,3,6,3,0,15,17,5,1,0,0,16,15,1,0,
  	0,0,17,20,1,0,0,0,18,16,1,0,0,0,18,19,1,0,0,0,19,22,1,0,0,0,20,18,1,0,
  	0,0,21,14,1,0,0,0,22,25,1,0,0,0,23,21,1,0,0,0,23,24,1,0,0,0,24,26,1,0,
  	0,0,25,23,1,0,0,0,26,27,5,0,0,1,27,5,1,0,0,0,28,29,3,2,1,0,29,30,5,2,
  	0,0,30,31,3,8,4,0,31,7,1,0,0,0,32,33,6,4,-1,0,33,34,5,7,0,0,34,44,3,8,
  	4,7,35,36,5,8,0,0,36,44,3,8,4,6,37,38,5,9,0,0,38,39,3,8,4,0,39,40,5,5,
  	0,0,40,44,1,0,0,0,41,44,3,2,1,0,42,44,3,0,0,0,43,32,1,0,0,0,43,35,1,0,
  	0,0,43,37,1,0,0,0,43,41,1,0,0,0,43,42,1,0,0,0,44,63,1,0,0,0,45,46,10,
  	8,0,0,46,47,5,6,0,0,47,62,3,8,4,9,48,49,10,5,0,0,49,50,5,8,0,0,50,62,
  	3,8,4,6,51,52,10,4,0,0,52,53,5,7,0,0,53,62,3,8,4,5,54,55,10,10,0,0,55,
  	62,5,3,0,0,56,57,10,9,0,0,57,58,5,4,0,0,58,59,3,8,4,0,59,60,5,5,0,0,60,
  	62,1,0,0,0,61,45,1,0,0,0,61,48,1,0,0,0,61,51,1,0,0,0,61,54,1,0,0,0,61,
  	56,1,0,0,0,62,65,1,0,0,0,63,61,1,0,0,0,63,64,1,0,0,0,64,9,1,0,0,0,65,
  	63,1,0,0,0,5,18,23,43,61,63
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  eigenexpParserStaticData = staticData.release();
}

}

EigenExpParser::EigenExpParser(TokenStream *input) : EigenExpParser(input, antlr4::atn::ParserATNSimulatorOptions()) {}

EigenExpParser::EigenExpParser(TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options) : Parser(input) {
  EigenExpParser::initialize();
  _interpreter = new atn::ParserATNSimulator(this, *eigenexpParserStaticData->atn, eigenexpParserStaticData->decisionToDFA, eigenexpParserStaticData->sharedContextCache, options);
}

EigenExpParser::~EigenExpParser() {
  delete _interpreter;
}

const atn::ATN& EigenExpParser::getATN() const {
  return *eigenexpParserStaticData->atn;
}

std::string EigenExpParser::getGrammarFileName() const {
  return "EigenExp.g4";
}

const std::vector<std::string>& EigenExpParser::getRuleNames() const {
  return eigenexpParserStaticData->ruleNames;
}

const dfa::Vocabulary& EigenExpParser::getVocabulary() const {
  return eigenexpParserStaticData->vocabulary;
}

antlr4::atn::SerializedATNView EigenExpParser::getSerializedATN() const {
  return eigenexpParserStaticData->serializedATN;
}


//----------------- ScalarValueContext ------------------------------------------------------------------

EigenExpParser::ScalarValueContext::ScalarValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t EigenExpParser::ScalarValueContext::getRuleIndex() const {
  return EigenExpParser::RuleScalarValue;
}

void EigenExpParser::ScalarValueContext::copyFrom(ScalarValueContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ScalarValueIDContext ------------------------------------------------------------------

tree::TerminalNode* EigenExpParser::ScalarValueIDContext::NUMBER() {
  return getToken(EigenExpParser::NUMBER, 0);
}

EigenExpParser::ScalarValueIDContext::ScalarValueIDContext(ScalarValueContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ScalarValueIDContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitScalarValueID(this);
  else
    return visitor->visitChildren(this);
}
EigenExpParser::ScalarValueContext* EigenExpParser::scalarValue() {
  ScalarValueContext *_localctx = _tracker.createInstance<ScalarValueContext>(_ctx, getState());
  enterRule(_localctx, 0, EigenExpParser::RuleScalarValue);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    _localctx = _tracker.createInstance<EigenExpParser::ScalarValueIDContext>(_localctx);
    enterOuterAlt(_localctx, 1);
    setState(10);
    match(EigenExpParser::NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MatrixContext ------------------------------------------------------------------

EigenExpParser::MatrixContext::MatrixContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t EigenExpParser::MatrixContext::getRuleIndex() const {
  return EigenExpParser::RuleMatrix;
}

void EigenExpParser::MatrixContext::copyFrom(MatrixContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- MatrixIDContext ------------------------------------------------------------------

tree::TerminalNode* EigenExpParser::MatrixIDContext::MATRIX_ID() {
  return getToken(EigenExpParser::MATRIX_ID, 0);
}

EigenExpParser::MatrixIDContext::MatrixIDContext(MatrixContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::MatrixIDContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitMatrixID(this);
  else
    return visitor->visitChildren(this);
}
EigenExpParser::MatrixContext* EigenExpParser::matrix() {
  MatrixContext *_localctx = _tracker.createInstance<MatrixContext>(_ctx, getState());
  enterRule(_localctx, 2, EigenExpParser::RuleMatrix);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    _localctx = _tracker.createInstance<EigenExpParser::MatrixIDContext>(_localctx);
    enterOuterAlt(_localctx, 1);
    setState(12);
    match(EigenExpParser::MATRIX_ID);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignsContext ------------------------------------------------------------------

EigenExpParser::AssignsContext::AssignsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t EigenExpParser::AssignsContext::getRuleIndex() const {
  return EigenExpParser::RuleAssigns;
}

void EigenExpParser::AssignsContext::copyFrom(AssignsContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- AssignmentsContext ------------------------------------------------------------------

tree::TerminalNode* EigenExpParser::AssignmentsContext::EOF() {
  return getToken(EigenExpParser::EOF, 0);
}

std::vector<EigenExpParser::AssignmentContext *> EigenExpParser::AssignmentsContext::assignment() {
  return getRuleContexts<EigenExpParser::AssignmentContext>();
}

EigenExpParser::AssignmentContext* EigenExpParser::AssignmentsContext::assignment(size_t i) {
  return getRuleContext<EigenExpParser::AssignmentContext>(i);
}

EigenExpParser::AssignmentsContext::AssignmentsContext(AssignsContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::AssignmentsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitAssignments(this);
  else
    return visitor->visitChildren(this);
}
EigenExpParser::AssignsContext* EigenExpParser::assigns() {
  AssignsContext *_localctx = _tracker.createInstance<AssignsContext>(_ctx, getState());
  enterRule(_localctx, 4, EigenExpParser::RuleAssigns);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    _localctx = _tracker.createInstance<EigenExpParser::AssignmentsContext>(_localctx);
    enterOuterAlt(_localctx, 1);
    setState(23);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == EigenExpParser::MATRIX_ID) {
      setState(14);
      assignment();
      setState(18);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == EigenExpParser::T__0) {
        setState(15);
        match(EigenExpParser::T__0);
        setState(20);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(25);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(26);
    match(EigenExpParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentContext ------------------------------------------------------------------

EigenExpParser::AssignmentContext::AssignmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t EigenExpParser::AssignmentContext::getRuleIndex() const {
  return EigenExpParser::RuleAssignment;
}

void EigenExpParser::AssignmentContext::copyFrom(AssignmentContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ExpAssignContext ------------------------------------------------------------------

EigenExpParser::MatrixContext* EigenExpParser::ExpAssignContext::matrix() {
  return getRuleContext<EigenExpParser::MatrixContext>(0);
}

EigenExpParser::ExpressionContext* EigenExpParser::ExpAssignContext::expression() {
  return getRuleContext<EigenExpParser::ExpressionContext>(0);
}

EigenExpParser::ExpAssignContext::ExpAssignContext(AssignmentContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpAssignContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpAssign(this);
  else
    return visitor->visitChildren(this);
}
EigenExpParser::AssignmentContext* EigenExpParser::assignment() {
  AssignmentContext *_localctx = _tracker.createInstance<AssignmentContext>(_ctx, getState());
  enterRule(_localctx, 6, EigenExpParser::RuleAssignment);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    _localctx = _tracker.createInstance<EigenExpParser::ExpAssignContext>(_localctx);
    enterOuterAlt(_localctx, 1);
    setState(28);
    matrix();
    setState(29);
    match(EigenExpParser::T__1);
    setState(30);
    expression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

EigenExpParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t EigenExpParser::ExpressionContext::getRuleIndex() const {
  return EigenExpParser::RuleExpression;
}

void EigenExpParser::ExpressionContext::copyFrom(ExpressionContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ExpParenthContext ------------------------------------------------------------------

EigenExpParser::ExpressionContext* EigenExpParser::ExpParenthContext::expression() {
  return getRuleContext<EigenExpParser::ExpressionContext>(0);
}

EigenExpParser::ExpParenthContext::ExpParenthContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpParenthContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpParenth(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpMatrixContext ------------------------------------------------------------------

EigenExpParser::MatrixContext* EigenExpParser::ExpMatrixContext::matrix() {
  return getRuleContext<EigenExpParser::MatrixContext>(0);
}

EigenExpParser::ExpMatrixContext::ExpMatrixContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpMatrixContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpMatrix(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpCholeskContext ------------------------------------------------------------------

std::vector<EigenExpParser::ExpressionContext *> EigenExpParser::ExpCholeskContext::expression() {
  return getRuleContexts<EigenExpParser::ExpressionContext>();
}

EigenExpParser::ExpressionContext* EigenExpParser::ExpCholeskContext::expression(size_t i) {
  return getRuleContext<EigenExpParser::ExpressionContext>(i);
}

EigenExpParser::ExpCholeskContext::ExpCholeskContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpCholeskContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpCholesk(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpAddContext ------------------------------------------------------------------

std::vector<EigenExpParser::ExpressionContext *> EigenExpParser::ExpAddContext::expression() {
  return getRuleContexts<EigenExpParser::ExpressionContext>();
}

EigenExpParser::ExpressionContext* EigenExpParser::ExpAddContext::expression(size_t i) {
  return getRuleContext<EigenExpParser::ExpressionContext>(i);
}

EigenExpParser::ExpAddContext::ExpAddContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpAddContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpAdd(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpPosContext ------------------------------------------------------------------

EigenExpParser::ExpressionContext* EigenExpParser::ExpPosContext::expression() {
  return getRuleContext<EigenExpParser::ExpressionContext>(0);
}

EigenExpParser::ExpPosContext::ExpPosContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpPosContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpPos(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpScalarValueContext ------------------------------------------------------------------

EigenExpParser::ScalarValueContext* EigenExpParser::ExpScalarValueContext::scalarValue() {
  return getRuleContext<EigenExpParser::ScalarValueContext>(0);
}

EigenExpParser::ExpScalarValueContext::ExpScalarValueContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpScalarValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpScalarValue(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpTransContext ------------------------------------------------------------------

EigenExpParser::ExpressionContext* EigenExpParser::ExpTransContext::expression() {
  return getRuleContext<EigenExpParser::ExpressionContext>(0);
}

EigenExpParser::ExpTransContext::ExpTransContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpTransContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpTrans(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpNegContext ------------------------------------------------------------------

EigenExpParser::ExpressionContext* EigenExpParser::ExpNegContext::expression() {
  return getRuleContext<EigenExpParser::ExpressionContext>(0);
}

EigenExpParser::ExpNegContext::ExpNegContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpNegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpNeg(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpSubContext ------------------------------------------------------------------

std::vector<EigenExpParser::ExpressionContext *> EigenExpParser::ExpSubContext::expression() {
  return getRuleContexts<EigenExpParser::ExpressionContext>();
}

EigenExpParser::ExpressionContext* EigenExpParser::ExpSubContext::expression(size_t i) {
  return getRuleContext<EigenExpParser::ExpressionContext>(i);
}

EigenExpParser::ExpSubContext::ExpSubContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpSubContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpSub(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ExpMultContext ------------------------------------------------------------------

std::vector<EigenExpParser::ExpressionContext *> EigenExpParser::ExpMultContext::expression() {
  return getRuleContexts<EigenExpParser::ExpressionContext>();
}

EigenExpParser::ExpressionContext* EigenExpParser::ExpMultContext::expression(size_t i) {
  return getRuleContext<EigenExpParser::ExpressionContext>(i);
}

EigenExpParser::ExpMultContext::ExpMultContext(ExpressionContext *ctx) { copyFrom(ctx); }


std::any EigenExpParser::ExpMultContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<EigenExpVisitor*>(visitor))
    return parserVisitor->visitExpMult(this);
  else
    return visitor->visitChildren(this);
}

EigenExpParser::ExpressionContext* EigenExpParser::expression() {
   return expression(0);
}

EigenExpParser::ExpressionContext* EigenExpParser::expression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  EigenExpParser::ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, parentState);
  EigenExpParser::ExpressionContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 8;
  enterRecursionRule(_localctx, 8, EigenExpParser::RuleExpression, precedence);

    

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(43);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case EigenExpParser::T__6: {
        _localctx = _tracker.createInstance<ExpNegContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;

        setState(33);
        match(EigenExpParser::T__6);
        setState(34);
        expression(7);
        break;
      }

      case EigenExpParser::T__7: {
        _localctx = _tracker.createInstance<ExpPosContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(35);
        match(EigenExpParser::T__7);
        setState(36);
        expression(6);
        break;
      }

      case EigenExpParser::T__8: {
        _localctx = _tracker.createInstance<ExpParenthContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(37);
        match(EigenExpParser::T__8);
        setState(38);
        expression(0);
        setState(39);
        match(EigenExpParser::T__4);
        break;
      }

      case EigenExpParser::MATRIX_ID: {
        _localctx = _tracker.createInstance<ExpMatrixContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(41);
        matrix();
        break;
      }

      case EigenExpParser::NUMBER: {
        _localctx = _tracker.createInstance<ExpScalarValueContext>(_localctx);
        _ctx = _localctx;
        previousContext = _localctx;
        setState(42);
        scalarValue();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    _ctx->stop = _input->LT(-1);
    setState(63);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(61);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<ExpMultContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(45);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(46);
          match(EigenExpParser::T__5);
          setState(47);
          expression(9);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<ExpAddContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(48);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(49);
          match(EigenExpParser::T__7);
          setState(50);
          expression(6);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<ExpSubContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(51);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(52);
          match(EigenExpParser::T__6);
          setState(53);
          expression(5);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<ExpTransContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(54);

          if (!(precpred(_ctx, 10))) throw FailedPredicateException(this, "precpred(_ctx, 10)");
          setState(55);
          match(EigenExpParser::T__2);
          break;
        }

        case 5: {
          auto newContext = _tracker.createInstance<ExpCholeskContext>(_tracker.createInstance<ExpressionContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleExpression);
          setState(56);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(57);
          match(EigenExpParser::T__3);
          setState(58);
          expression(0);
          setState(59);
          match(EigenExpParser::T__4);
          break;
        }

        default:
          break;
        } 
      }
      setState(65);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

bool EigenExpParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 4: return expressionSempred(antlrcpp::downCast<ExpressionContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool EigenExpParser::expressionSempred(ExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 8);
    case 1: return precpred(_ctx, 5);
    case 2: return precpred(_ctx, 4);
    case 3: return precpred(_ctx, 10);
    case 4: return precpred(_ctx, 9);

  default:
    break;
  }
  return true;
}

void EigenExpParser::initialize() {
  ::antlr4::internal::call_once(eigenexpParserOnceFlag, eigenexpParserInitialize);
}
