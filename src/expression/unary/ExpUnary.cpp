#include "expression/unary/ExpUnary.hpp"
#include "expression/DerivativeType.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "values/MatrixCache.hpp"


#include "expression/Expression.hpp"



ExpUnary::ExpUnary(std::shared_ptr<Expression> exp) {
	exp_ = exp;
	setAndIncrementID();
}

ExpUnary::ExpUnary(std::shared_ptr<Expression> exp, ExpressionResultType resultType) {
	exp_ = exp;
	resultType_ = resultType;
	setAndIncrementID();
}


void ExpUnary::generateMatrixValues(MatrixCache& matrixValuesCache) {
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		exp_->generateMatrixValues(matrixValuesCache);
	}
}

void ExpUnary::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache){
	ExpressionResultType expRT = exp_->getExpressionResultType();
	if (expRT == ExpressionResultType::Matrix) {
		exp_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
}

void ExpUnary::devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache){
	exp_->devideIntoSubexpressions(subexpressions, matrixValuesCache);
}
