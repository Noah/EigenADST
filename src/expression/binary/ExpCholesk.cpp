#include "expression/binary/ExpCholesk.hpp"

#include <memory>
#include <string>
#include <vector>
#include <variant>

#include "EigenNS.hpp"
#include "expression/terminal/ExpMatrix.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/binary/ExpMult.hpp"
#include "expression/unary/ExpNeg.hpp"
#include "expression/Expression.hpp"
#include "dimension/DimGraph.hpp"
#include "expression/ExpAssign.hpp"
#include "values/MatrixCache.hpp"
#include "exception/EigenADException.hpp"
#include "expression/binary/ExpSub.hpp"
#include "expression/unary/ExpTrans.hpp"
#include "dimension/Dimension.hpp"
#include "dimension/DimNodeCholesk.hpp"
#include "expression/binary/ExpLowUpp.hpp"



ExpCholesk::ExpCholesk(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right, ExpressionResultType resultType, std::string nameSubst, std::shared_ptr<Dimension> row,
	std::shared_ptr<Dimension> column, Eigen::LLT<MatrixType> lltValue, MatrixType generatedLeftValues) {
	left_ = left;
	right_ = right;
	resultType_ = resultType;
	nameSubstitution_ = nameSubst;
	row_ = row;
	column_ = column;
	lltValue_ = lltValue;
	generatedLeftValues_ = generatedLeftValues;
	setAndIncrementID();
}

std::string ExpCholesk::toString() const {
	if (nameSubstitution_ == NO_CHOLESK_SUBST || !optimizeOutput) {
		return toStringFull();
	}
	return "(" + nameSubstitution_ + ").solve( " + right_->toString() + " )";
}

std::string ExpCholesk::toStringFull() const {
	return "(" + left_->toString() + ").llt().solve( " + right_->toString() + " )";
}

std::shared_ptr<Expression> ExpCholesk::tangDifferentiate() const {

	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		std::shared_ptr<ExpCholesk> leftCholesk = std::make_shared<ExpCholesk>(left_->deepCopy(), right_->tangDifferentiate(), resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);

		std::shared_ptr<ExpCholesk> choleskCopy = std::make_shared<ExpCholesk>(left_->deepCopy(), right_->deepCopy(), resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);

		std::shared_ptr<ExpMult> rMult = std::make_shared<ExpMult>(left_->tangDifferentiate(), choleskCopy, resultType_);

		std::shared_ptr<ExpCholesk> rightCholesk = std::make_shared<ExpCholesk>(left_->deepCopy(), rMult, resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);

		std::shared_ptr<ExpSub> mainSub = std::make_shared<ExpSub>(leftCholesk, rightCholesk, resultType_);
		return mainSub;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toStringFull(), "tangDifferentiate()");
	}
}

void ExpCholesk::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {

	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		std::shared_ptr<ExpCholesk> choleskCopy = std::make_shared<ExpCholesk>(left_->deepCopy(), right_->deepCopy(), resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);

		std::shared_ptr<ExpTrans> trans = std::make_shared<ExpTrans>(choleskCopy, resultType_);
		std::shared_ptr<ExpMult> lMult = std::make_shared<ExpMult>(tempAdj, trans, resultType_);
		std::shared_ptr<ExpCholesk> leftCholesk = std::make_shared<ExpCholesk>(left_->deepCopy(), lMult, resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);
		std::shared_ptr<ExpNeg> tempAdjLeft = std::make_shared<ExpNeg>(leftCholesk, resultType_);
		left_->adjDifferentiate(tempAdjLeft, allAdjoints);

		std::shared_ptr<ExpCholesk> tempAdjRight = std::make_shared<ExpCholesk>(left_->deepCopy(), tempAdj->deepCopy(), resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);
		right_->adjDifferentiate(tempAdjRight, allAdjoints);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toStringFull(), "adjDifferentiate(...)");
	}
}

void ExpCholesk::setDimensionNodes(DimGraph& graph) {

	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		graph.connectToAndAddIfAbsent(left_->createColDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(right_->createColDimNode(), getHashID(), DimensionType::Column);
		graph.addIfAbsentAndConnect(left_->createColDimNode(), left_->createRowDimNode());
		graph.addIfAbsentAndConnect(left_->createRowDimNode(), right_->createRowDimNode());


		left_->setDimensionNodes(graph);
		right_->setDimensionNodes(graph);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toStringFull(), "setDimensionNodes(...)");
	}
}

void ExpCholesk::generateMatrixValues(MatrixCache& matrixValuesCache) {
	left_->generateMatrixValues(matrixValuesCache);
	right_->generateMatrixValues(matrixValuesCache);
	std::string leftToString = left_->toString();
	if (matrixValuesCache.containsLLTKey(leftToString)) {
		auto pair = matrixValuesCache.getLLTValuesOf(leftToString);
		generatedLeftValues_ = pair.first;
		lltValue_ = pair.second;
		nameSubstitution_ = LLT_SUBST + std::to_string(matrixValuesCache.getLLTNameSubstCounterOf(leftToString));
	}
	else {
		//use row_->getSize() because row_ and column_ are the dimensions of the result, however we set a newResult for the left subexpression.
		//Because A.llt().solve(B)=C solves A*C=B the row_ of C is also the same size of the row of A (A is sqaure (n,n) and A*C implies for C (n,m))
		MatrixType newResult = MatrixType::Random(row_->getSize(), row_->getSize());
		newResult = newResult * newResult.transpose();
		generatedLeftValues_ = newResult;
		lltValue_ = newResult.llt();
		nameSubstitution_ = LLT_SUBST + std::to_string(matrixValuesCache.addLLTEntry(leftToString, generatedLeftValues_, lltValue_));
	}
}

std::variant<ScalarValueType, MatrixType> ExpCholesk::calculate() const {

	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		
		MatrixType leftResult = std::get<MatrixType>(left_->calculate());

		if (leftResult.isZero(EPSILON)) {
			throw CholeskyFactorizationZeroInput(getExpressionType(), toStringFull(), "calculate()");
		}		
		MatrixType res = lltValue_.solve(std::get<MatrixType>(right_->calculate()));

		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toStringFull(), "calculate()");
	}
	
}

std::shared_ptr<Expression> ExpCholesk::deepCopy() const {
	std::shared_ptr<Expression> lCopy = left_->deepCopy();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpCholesk>(lCopy, rCopy, resultType_, nameSubstitution_, row_, column_, lltValue_, generatedLeftValues_);
}

ExpressionResultType ExpCholesk::checkSemantics() {
	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		return resultType_ = ExpressionResultType::Matrix;
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), {leftRT, rightRT}, getRequiredTypes(), toStringFull(), "checkSemantics()");
	}


}

std::vector<std::vector<ExpressionResultType>> ExpCholesk::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix, ExpressionResultType::Matrix} };
}

ExpressionType ExpCholesk::getExpressionType() const {
	return ExpressionType::Cholesky_Factorization;
}

void ExpCholesk::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache){
	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {

		throw CallOfUndefinedFunctionException(getExpressionType(), "perturbeMatrixValuesBy(...)", "Cannot perturbe matrix values in subexpressions, because objects of this class requires a symmetric positive-definite matrix.");
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toStringFull(), "checkSemantics()");
	}
}

std::shared_ptr<DimNode> ExpCholesk::createRowDimNode() const {
	return std::make_shared<DimNodeCholesk>(getHashID(), DimensionType::Row, row_);
}

std::shared_ptr<DimNode> ExpCholesk::createColDimNode() const {
	return std::make_shared<DimNodeCholesk>(getHashID(), DimensionType::Column, column_);
}

void ExpCholesk::devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache) {
	left_->devideIntoSubexpressions(subexpressions, matrixValuesCache);
	right_->devideIntoSubexpressions(subexpressions, matrixValuesCache);

	std::string leftStringRepresentation = left_->toString();
	for (auto& exp : subexpressions) {
		if (leftStringRepresentation == exp->getRight()->toString()) {
			//subexpression does exist
			left_ = exp->getLeft()->deepCopy();
			return;
		}
	}
	//subexpression is not already added
	std::shared_ptr<ExpMatrix> subExpMat = std::make_shared<ExpMatrix>("LLTSubstitutionMatrix1:" + std::to_string(id_), "", DerivativeType::Primal, 0, false, false, ExpressionResultType::Matrix,
		row_, row_, generatedLeftValues_); //pass row_ row_ because subExpMat describes substitution value for left subexpression
	
	matrixValuesCache.addInputEntry(subExpMat->toString(), subExpMat->getName(), subExpMat->getSuffixes(), subExpMat->getIteration(), DerivativeType::Primal, generatedLeftValues_, row_->toString(), column_->toString());
	matrixValuesCache.addLLTEntry(subExpMat->toString(), generatedLeftValues_, lltValue_);

	std::shared_ptr<ExpAssign> subexpr = std::make_shared<ExpAssign>(subExpMat, left_->deepCopy(), DerivativeType::Primal, ExpressionResultType::Matrix);
	subexpressions.push_back(subexpr);
	left_ = subExpMat->deepCopy();
}

std::shared_ptr<Expression> ExpCholesk::transformExpCholeskToExpLowUpp() const{
	return std::make_shared<ExpLowUpp>(left_->transformExpCholeskToExpLowUpp(), right_->transformExpCholeskToExpLowUpp(), resultType_);
}
