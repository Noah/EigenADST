#include "expression/binary/ExpBinary.hpp"
#include "expression/Expression.hpp"
#include <string>
#include <memory>
#include <vector>

#include "values/MatrixCache.hpp"
#include "expression/ExpressionResultType.hpp"
#include <iostream>

class MatrixCache;

void ExpBinary::generateMatrixValues(MatrixCache& matrixValuesCache) {
	
	if (left_->getExpressionResultType() == ExpressionResultType::Matrix) {
		left_->generateMatrixValues(matrixValuesCache);
	}
	if (right_->getExpressionResultType() == ExpressionResultType::Matrix) {
		right_->generateMatrixValues(matrixValuesCache);
	}
}

ExpBinary::ExpBinary(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right) {
	left_ = left;
	right_ = right;
	setAndIncrementID();
}

ExpBinary::ExpBinary(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right, ExpressionResultType resultType) {
	left_ = left;
	right_ = right;
	resultType_ = resultType;
	setAndIncrementID();
}

void ExpBinary::devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache){
	left_->devideIntoSubexpressions(subexpressions, matrixValuesCache);
	right_->devideIntoSubexpressions(subexpressions, matrixValuesCache);
}
