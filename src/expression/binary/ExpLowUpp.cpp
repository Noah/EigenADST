#include "expression/binary/ExpLowUpp.hpp"

#include <memory>
#include <string>
#include <vector>
#include <variant>

#include "EigenNS.hpp"
#include "expression/terminal/ExpMatrix.hpp"
#include "expression/ExpressionType.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/binary/ExpMult.hpp"
#include "expression/unary/ExpNeg.hpp"
#include "expression/Expression.hpp"
#include "dimension/DimGraph.hpp"
#include "expression/ExpAssign.hpp"
#include "values/MatrixCache.hpp"
#include "exception/EigenADException.hpp"
#include "expression/binary/ExpSub.hpp"
#include "expression/unary/ExpTrans.hpp"
#include "dimension/Dimension.hpp"


std::string ExpLowUpp::toString() const {
	return "(" + left_->toString() + ").lu().solve( " + right_->toString() + " )";
}

std::shared_ptr<Expression> ExpLowUpp::tangDifferentiate() const {

	throw CallOfUndefinedFunctionException(getExpressionType(), "tangDifferentiate()", "Symbolic derivatives not possible.");
	
}

void ExpLowUpp::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const
{
	throw CallOfUndefinedFunctionException(getExpressionType(), "adjDifferentiate(...)", "Symbolic derivatives not possible.");
}

void ExpLowUpp::setDimensionNodes(DimGraph& graph)
{
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		graph.connectToAndAddIfAbsent(left_->createColDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(right_->createColDimNode(), getHashID(), DimensionType::Column);
		graph.addIfAbsentAndConnect(left_->createColDimNode(), left_->createRowDimNode());
		graph.addIfAbsentAndConnect(left_->createRowDimNode(), right_->createRowDimNode());


		left_->setDimensionNodes(graph);
		right_->setDimensionNodes(graph);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}

std::variant<ScalarValueType, MatrixType> ExpLowUpp::calculate() const
{
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {

		MatrixType leftResult = std::get<MatrixType>(left_->calculate());

		if (leftResult.isZero(EPSILON)) {
			throw CholeskyFactorizationZeroInput(getExpressionType(), toString(), "calculate()");
		}
		MatrixType res = leftResult.lu().solve(std::get<MatrixType>(right_->calculate()));

		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpLowUpp::deepCopy() const
{
	std::shared_ptr<Expression> lCopy = left_->deepCopy();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpLowUpp>(lCopy, rCopy, resultType_);
}

ExpressionResultType ExpLowUpp::checkSemantics()
{
	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		return resultType_ = ExpressionResultType::Matrix;
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

std::vector<std::vector<ExpressionResultType>> ExpLowUpp::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix, ExpressionResultType::Matrix} };
}

ExpressionType ExpLowUpp::getExpressionType() const {
	return ExpressionType::Lower_Upper_Factorization;
}

void ExpLowUpp::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache)
{
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		left_->perturbeMatrixValuesBy(amount, matrixValueCache);
		right_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

void ExpLowUpp::devideIntoSubexpressions(std::vector<std::shared_ptr<ExpAssign>>& subexpressions, MatrixCache& matrixValuesCache)
{
	throw CallOfUndefinedFunctionException(getExpressionType(), "devideIntoSubexpressions()", "Objects of type ExpCholesk get converted into ExpLowUpp objects.");
}

std::shared_ptr<Expression> ExpLowUpp::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpLowUpp>(left_->transformExpCholeskToExpLowUpp(), right_->transformExpCholeskToExpLowUpp(), resultType_);
}