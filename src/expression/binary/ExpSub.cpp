#include "expression/binary/ExpSub.hpp"
#include <memory>
#include <string>
#include <vector>

#include "expression/Expression.hpp"
#include "expression/unary/ExpNeg.hpp"
#include "expression/ExpressionResultType.hpp"
#include "expression/ExpressionType.hpp"
#include "values/MatrixCache.hpp"
#include "exception/EigenADException.hpp"
#include "dimension/DimGraph.hpp"


std::string ExpSub::toString() const {
	return "(" + left_->toString() + " - " + right_->toString() + ")";
}

std::shared_ptr<Expression> ExpSub::tangDifferentiate() const{
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {

		return std::make_shared<ExpSub>(left_->tangDifferentiate(), right_->tangDifferentiate(), resultType_);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		return deepCopy();
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "tangDifferentiate()");
	}	
}

void ExpSub::adjDifferentiate(const std::shared_ptr<Expression>& tempAdj, std::vector<std::shared_ptr<ExpAssign>>& allAdjoints) const {

	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		//propagate function to both children, copy tempAdj
		left_->adjDifferentiate(tempAdj->deepCopy(), allAdjoints);
		std::shared_ptr<ExpNeg> negation = std::make_shared<ExpNeg>(tempAdj, resultType_);
		right_->adjDifferentiate(negation, allAdjoints);
	}
	else if(leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar){
		//only Scalar return types -> no further matrices -> no further propagation
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "adjDifferentiate(...)");
	}
	
}

std::variant<ScalarValueType, MatrixType> ExpSub::calculate() const {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	std::variant<ScalarValueType, MatrixType> res;
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		res = std::get<MatrixType>(left_->calculate()) - std::get<MatrixType>(right_->calculate());
		return res;
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//Scalar
		res = std::get<ScalarValueType>(left_->calculate()) - std::get<ScalarValueType>(right_->calculate());
		return res;
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

void ExpSub::setDimensionNodes(DimGraph& graph) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();

	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		graph.connectToAndAddIfAbsent(left_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(left_->createColDimNode(), getHashID(), DimensionType::Column);

		graph.connectToAndAddIfAbsent(right_->createRowDimNode(), getHashID(), DimensionType::Row);
		graph.connectToAndAddIfAbsent(right_->createColDimNode(), getHashID(), DimensionType::Column);

		left_->setDimensionNodes(graph);
		right_->setDimensionNodes(graph);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
		//only Scalar return types -> no further matrices -> no further propagation
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "setDimensionNodes(...)");
	}
}

std::shared_ptr<Expression> ExpSub::deepCopy() const {
	std::shared_ptr<Expression> lCopy = left_->deepCopy();
	std::shared_ptr<Expression> rCopy = right_->deepCopy();
	return std::make_shared<ExpSub>(lCopy, rCopy, resultType_);
}

ExpressionResultType ExpSub::checkSemantics() {

	ExpressionResultType leftRT = left_->checkSemantics();
	ExpressionResultType rightRT = right_->checkSemantics();

	if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {

		return resultType_ = ExpressionResultType::Scalar;
	}
	else if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {

		return resultType_ = ExpressionResultType::Matrix;
	}
	else {
		throw InvalidMatrixExpressionException(getExpressionType(), {leftRT, rightRT}, getRequiredTypes(), toString(), "checkSemantics()");
	}
}

std::vector<std::vector<ExpressionResultType>> ExpSub::getRequiredTypes() const {
	return { {ExpressionResultType::Matrix, ExpressionResultType::Matrix}, {ExpressionResultType::Scalar, ExpressionResultType::Scalar} };
}

ExpressionType ExpSub::getExpressionType() const {
	return ExpressionType::Subtraction;
}

void ExpSub::perturbeMatrixValuesBy(ScalarValueType amount, const MatrixCache& matrixValueCache) {
	ExpressionResultType leftRT = left_->getExpressionResultType();
	ExpressionResultType rightRT = right_->getExpressionResultType();
	if (leftRT == ExpressionResultType::Matrix && rightRT == ExpressionResultType::Matrix) {
		left_->perturbeMatrixValuesBy(amount, matrixValueCache);
		right_->perturbeMatrixValuesBy(amount, matrixValueCache);
	}
	else if (leftRT == ExpressionResultType::Scalar && rightRT == ExpressionResultType::Scalar) {
	}
	else {
		throw InvalidResultTypeException(getExpressionType(), { leftRT, rightRT }, getRequiredTypes(), toString(), "calculate()");
	}
}

std::shared_ptr<Expression> ExpSub::transformExpCholeskToExpLowUpp() const {
	return std::make_shared<ExpSub>(left_->transformExpCholeskToExpLowUpp(), right_->transformExpCholeskToExpLowUpp(), resultType_);
}