#include "GUI/innerPanels/AdjointsPanel.hpp"

#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/sizer.h>

#include "GUI/ButtonID.hpp"


AdjointsPanel::AdjointsPanel(wxWindow* parent) : wxPanel(parent) {
    adjDescription_ = new wxStaticText(this, wxID_ANY, "Adjoint Derivative(s)");
    copyAdjButton_ = new wxButton(this, COPY_ADJOINTS_BUTTON, "Copy to Clipboard");

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->AddStretchSpacer(10);
    sizer->Add(adjDescription_, 0, wxALIGN_CENTER | wxALL, 5);
    sizer->AddStretchSpacer(3);
    sizer->Add(copyAdjButton_, 0, wxALIGN_CENTER | wxALL, 5);
    sizer->AddStretchSpacer(10);

    this->SetSizer(sizer);
    this->Fit();
}


