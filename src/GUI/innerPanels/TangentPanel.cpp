#include "GUI/innerPanels/TangentPanel.hpp"

#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/sizer.h>

#include "GUI/ButtonID.hpp"

TangentPanel::TangentPanel(wxWindow* parent) : wxPanel(parent) {
    tangDescription_ = new wxStaticText(this, wxID_ANY, "Tangent Derivative");
    copyTangButton_ = new wxButton(this, COPY_TANGENT_BUTTON, "Copy to Clipboard");

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
    sizer->AddStretchSpacer(10);
    sizer->Add(tangDescription_, 0, wxALIGN_CENTER | wxALL, 5);
    sizer->AddStretchSpacer(3);
    sizer->Add(copyTangButton_, 0, wxALIGN_CENTER | wxALL, 5);
    sizer->AddStretchSpacer(10);

    this->SetSizer(sizer);
    this->Fit();
}


