#include "GUI/EigenADApp.hpp"
#include "GUI/MainFrame.hpp"

wxIMPLEMENT_APP(EigenADApp);

bool EigenADApp::OnInit() {
    MainFrame* frame = new MainFrame("EigenADST");
    frame->Show(true);
    return true;
}