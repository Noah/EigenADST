#include "dimension/DimGraph.hpp"

#include <vector>
#include <memory>
#include <random>
#include <iostream>
#include "dimension/DimensionType.hpp"

#include "dimension/DimNode.hpp"

#include <iostream>

constexpr const int MIN_DIM = 2; //exclusive
constexpr const int MAX_DIM = 9; //inclusive

DimGraph::DimGraph() {

}

std::string DimGraph::toString() const{
	std::string out;
	for (auto& node : allNodes_) {
		out = out + node->toString() + "\n";
	}
	for (auto& dim : allDimensions_) {
		out = out + dim.toString() + " " + std::to_string(dim.getSize()) + "\n";
	}
	return out;
}

std::string DimGraph::getDimensionDeclarations() const {
	std::string res = "//generated dimension declarations \n";
	res = res + "const int ";
	for (size_t i = 0; i < allDimensions_.size(); i++) {
		res = res + allDimensions_[i].toString() + " = " + std::to_string(allDimensions_[i].getSize());
		if (i != allDimensions_.size() - 1) {
			res = res + ", ";
		}
	}
	res = res + ";\n";
	return res;
}

void DimGraph::connectToAndAddIfAbsent(std::shared_ptr<DimNode> node, const std::string& toHashID,
	DimensionType toDimType) {

	//first get other node (exists in any case)
	std::shared_ptr<DimNode> other;
	for (auto& entry : allNodes_) {
		if (entry->getHashID() == toHashID && entry->getDimType() == toDimType) {
			other = entry;
			break;
		}
	}

	for (auto& entry : allNodes_) {
		//check if node already exist using hashID, if yes connect both and return
		if (entry->getHashID() == node->getHashID() && entry->getDimType() == node->getDimType()) {
			//node already exists: connect to other node:
			DimNode::connect(node, other);
			return;
		}
	}
	//node does not exist: add to list, connect 
	//note: node can be a DimNodeMatrix, in that case check if node must connect to other nodes that
	//	describe the same matrix or are derivatives of the same matrix (have the same name_)
	DimNode::connect(node, other);
	connectToEquiv(node);
	allNodes_.push_back(node);
}
#include <iostream>
void DimGraph::addIfAbsentAndConnect(std::shared_ptr<DimNode> first, std::shared_ptr<DimNode> second) {
	//check if first node already exists
	if (first->isIdenticalTo(second)) {
		std::cout << " !!! 1 !!! " << std::endl;
		std::cout << first->getHashID() << second->getHashID() << std::endl;
		addIfAbsent(first);
		return;
	}
	bool firstExists = false;
	bool secondExists = false;
	std::shared_ptr<DimNode> firstFound;
	std::shared_ptr<DimNode> secondFound;
	for (auto& entry : allNodes_) {
		if (!firstExists && entry->getHashID() == first->getHashID() && entry->getDimType() == first->getDimType()) {
			//does exist
			firstFound = entry;
			firstExists = true;
		}
		if (!secondExists && entry->getHashID() == second->getHashID() && entry->getDimType() == second->getDimType()) {
			//does exist
			secondFound = entry;
			secondExists = true;
		}
		if (firstExists && secondExists) {
			DimNode::connect(firstFound, secondFound);
			return;
		}
	}
	//at least one of the nodes does not exist:
	if (!firstExists && !secondExists) {
		//both do not exist

		connectToEquiv(first);
		connectToEquiv(second);
		allNodes_.push_back(first);
		allNodes_.push_back(second);
		DimNode::connect(first, second);
	}
	else if (firstExists && !secondExists) {
		//only first node exists
		connectToEquiv(second);
		allNodes_.push_back(second);
		DimNode::connect(firstFound, second);

	}
	else /* if (!firstExists && secondExists) */ {
		//only second node exists
		connectToEquiv(first);
		allNodes_.push_back(first);
		DimNode::connect(first, secondFound);
	}

}

void DimGraph::addIfAbsent(std::shared_ptr<DimNode> node) {
	for (auto& entry : allNodes_) {
		if (entry->getHashID() == node->getHashID() && entry->getDimType() == node->getDimType()) {
			//exists: do not add
			return;
		}
	}
	//does not exist: add and potentially connect to other DimNodeMatrix
	connectToEquiv(node);
	allNodes_.push_back(node);
}

void DimGraph::connectToEquiv(std::shared_ptr<DimNode> node) {
	if (node->getMatrixName() != NO_NAME) {
		for (auto& entry : allNodes_) {
			if (entry->getMatrixName() != NO_NAME) {
				if (entry->getMatrixName() == node->getMatrixName() && entry->getDimType() == node->getDimType()) {
					DimNode::connect(entry, node);
				}
			}
		}
	}
}

void DimGraph::generateMatrixDimensions() {
	int num = 1;

	std::random_device dev;
	std::mt19937 rng(dev());
	std::uniform_int_distribution<int> dist(MIN_DIM, MAX_DIM);

	for (auto& node : allNodes_) {
		if (!node->isDiscovered()) {
			
			Dimension dimension = Dimension(dist(rng), num);
			node->setDimOfConnected(dimension);
			allDimensions_.push_back(dimension);
			num++;
		}
	}
}