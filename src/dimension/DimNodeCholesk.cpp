#include "dimension/DimNodeCholesk.hpp"

#include <string>
#include <memory>
#include "dimension/DimensionType.hpp"
#include "dimension/Dimension.hpp"

DimNodeCholesk::DimNodeCholesk(const std::string& hashID, DimensionType dimType, std::shared_ptr<Dimension> dim) {
	hashID_ = hashID;
	dimType_ = dimType;
	dim_ = dim;
}

void DimNodeCholesk::setDimension(const Dimension& dim){
	dim_->setSize(dim.getSize());
	dim_->setNum(dim.getNum());
}
