#include "dimension/DimNodeMatrix.hpp"

#include <string>
#include <memory>

#include "dimension/DimensionType.hpp"


#include "dimension/Dimension.hpp"

DimNodeMatrix::DimNodeMatrix(const std::string& hashID, const std::string& name,
	DimensionType dimType, std::shared_ptr<Dimension> dim)
{
	hashID_ = hashID;
	matrixName_ = name;
	dimType_ = dimType;
	dim_ = dim;
}

void DimNodeMatrix::setDimension(const Dimension& dim) {
	dim_->setSize(dim.getSize());
	dim_->setNum(dim.getNum());
}