﻿///**
// * @file EigenADCP.cpp
// * @brief Main function of the Eigen expression evaluation and differentiation project .
// */
//
//// info@stce.rwth-aachen.de
//
//#include <iostream>
//
//#include "EigenNS.hpp"
//#include "expression/Expression.hpp"
//#include "expression/terminal/ExpMatrix.hpp"
//#include "exception/EigenADException.hpp"
//#include "EigenExpLexer.h"
//#include "EigenExpParser.h"
//#include "antlrToExp/ExpBuilder.hpp"
//#include "antlrToExp/ExpErrorListener.hpp"
//#include "dimension/DimGraph.hpp"
//#include "expression/ExpAssign.hpp"
//#include "values/MatrixCache.hpp"
//#include "expression/DerivativeType.hpp"
//
///**
// * @brief Command Line Debug Main function of the Eigen expression differentiation and validation project.
// * This function runs the main execution flow of the program, which involves parsing an Eigen matrix expression,
// * performing semantic checking, generating matrix dimensions, differentiating the expression (adjoint and tangent),
// * generating and printing matrix values (primal, adjoint, and tangent), validating derivatives, generating matrix
// * declarations, and printing the original and differentiated expressions.
// *
// * Any errors during the process are caught and printed to the standard output.
// *
// * @return int The exit status of the program.
// */
//int main() {
//    //TODO look into ExpCholesk::calculate(..)
//    std::string input = "A_t1=B_a1*C_t1.transpose().llt().solve(D_a1)*(-7*(B-C))";    //<- user input
//    int order = 2;                                                  //<- user input
//
//    antlr4::ANTLRInputStream inputStream(input);
//    EigenExpLexer lexer(&inputStream);
//    antlr4::CommonTokenStream tokenStream(&lexer);
//    EigenExpParser parser(&tokenStream);
//    ExpErrorListener errListener;
//    lexer.removeErrorListeners();
//    lexer.addErrorListener(&errListener);
//    parser.removeErrorListeners();
//    parser.addErrorListener(&errListener);
//    antlr4::tree::ParseTree* tree = parser.assigns();
//
//    if (!errListener.getHasError()) {
//        //std::cout << "Parse tree: " << tree->toStringTree(&parser) << std::endl;
//        ExpBuilder visitor;
//        std::vector<std::shared_ptr<ExpAssign>> result = std::any_cast<std::vector<std::shared_ptr<ExpAssign>>>(visitor.visit(tree));
//        for (std::shared_ptr<ExpAssign> exp : result) {
//            try
//            {
//                if (order <= visitor.getHighestOrder().second) {
//                    throw InvalidDerivativeOrderException(visitor.getHighestOrder(), order);
//                }
//                
//                Expression::setDerivativeOrder(order);
//                // Semantic Check
//                exp->checkSemantics();
//                DimGraph graph;
//                exp->setDimensionNodes(graph);
//                graph.generateMatrixDimensions(); //generates Dimensions for each Matrix in the original expression
//
//                std::cout << graph.toString() << std::endl;
//
//                std::vector<std::shared_ptr<ExpAssign>> allAdjoints;
//                exp->adjDifferentiate(nullptr, allAdjoints);
//               
//                std::shared_ptr<ExpAssign> tangD = exp->startTangDifferentiate();
//
//
//                std::cout << exp->toString() << " //primal" << std::endl;
//                for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
//                    std::cout << adj->toString() << " //adjoint" << std::endl;
//                }
//                std::cout << tangD->toString() << " //tangent" << std::endl;
//
//
//                //generate matrix values by giving a cache of existing (toString(), std::pair<MatrixType, type=Adj, Tang, Prim) hashlist
//                //then check if its toString() is in the cache
//                //if yes: use existing MatrixType
//                //if no: generate MatrixType based on Dimensions and add to cache
//                
//                MatrixCache matrixValuesCache;
//                exp->generateMatrixValues(matrixValuesCache); 
//                std::cout << "primal values generated" << std::endl;
//                
//
//                for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
//                    adj->generateMatrixValues(matrixValuesCache);
//                }
//                std::cout << "adjoint values generated" << std::endl;
//
//
//                tangD->generateMatrixValues(matrixValuesCache);
//                std::cout << "tangent values generated" << std::endl;
//
//
//                std::cout << "primal result: \n" << std::get<MatrixType>(exp->calculate()) << std::endl;
//                matrixValuesCache.addOutputEntry(exp->getLeft());
//                std::cout << "" << std::endl;
//                
//
//                for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
//                    std::cout << "adjoint result: \n" << std::get<MatrixType>(adj->calculate()) << std::endl;
//                    matrixValuesCache.addOutputEntry(adj->getLeft());
//                    std::cout << "" << std::endl;
//                    
//                }
//
//                std::cout << "tangent result: \n" << std::get<MatrixType>(tangD->calculate()) << std::endl;
//                matrixValuesCache.addOutputEntry(tangD->getLeft());
//
//                //validate derivatives:
//                //for a nested equation C:  C_t1.cwiseProduct(C_a1).sum() =
//                //  for each input variable A:  A_t1.cwiseProduct(A_a1).sum()
//                //
//                //use matrixValuesCache
//                // first get (C_t1) MatrixType of tangD->calculate() and the respective adjoint MatrixType
//                // then: for all other MatrixType that are in adjoint toString, find all other MatrixType that are in tangent (note that then C_a1) is the only one left
//
//                //matrixValuesCache contains now all primal Input Variables, all Tangent input Variables and
//                //  the adjoint output variable
//
//
//                //generate declarations for all matrices using cache (toString(), std::pair<Dimension, Dimension>)
//
//                //exp <- primal
//                //adjoints <- vector of adjoints
//                //tangD <- tangent         
//                matrixValuesCache.validateDuality();
//
//                std::cout << std::endl;
//
//                std::cout << graph.getDimensionDeclarations() << std::endl;
//                std::cout << matrixValuesCache.getMatrixDeclarations() << std::endl;
//
//                std::cout << exp->toString() << " //primal" << std::endl;
//                for (std::shared_ptr<ExpAssign>& adj : allAdjoints) {
//                    std::cout << adj->toString() << " //adjoint" << std::endl;
//                }
//                std::cout << tangD->toString() << " //tangent" << std::endl;
//            }
//            catch (const EigenADException& se)
//            {
//                std::cout << se.what() << std::endl;
//            }
//        }
//    }
//
//    //At();
//    //A_neg();
//    //aBpC();
//    //aBmC();
//    //AB();
//    //AXeB_LLT();
//    return 0;
//}
//
//// transpose
//void At() {
//    const int n = 3, m = 2;
//    // primal
//    MatrixType A = MatrixType::Random(n, m), B;
//    B = A.transpose();
//   std::cout << B << std::endl;
//
//    // tangent
//    MatrixType A_t1 = MatrixType::Random(n, m), B_t1;
//    B_t1 = A_t1.transpose();
//   std::cout << B_t1 << std::endl;
//
//    // adjoint
//    MatrixType A_a1 = MatrixType::Zero(n, m), B_a1 = MatrixType::Random(m, n);
//    A_a1 += B_a1.transpose();
//   std::cout << A_a1 << std::endl;
//
//    // validation via invariant
//    // see also https://arxiv.org/abs/2101.03334
//   std::cout << B_t1.cwiseProduct(B_a1).sum() << "=?="
//        << A_t1.cwiseProduct(A_a1).sum() << std::endl;
//
//}
//
//// unary minus
//void A_neg() {
//    const int n = 3, m = 2;
//    // primal
//    MatrixType A = MatrixType::Random(n, m), B(n, m);
//    B = -A;
//   std::cout << B << std::endl;
//
//    // tangent
//    MatrixType A_t1 = MatrixType::Random(n, m), B_t1(n, m);
//    B_t1 = -A_t1;
//   std::cout << B_t1 << std::endl;
//
//    // adjoint
//    MatrixType A_a1 = MatrixType::Zero(n, m), B_a1 = MatrixType::Random(n, m);
//    A_a1 += -B_a1;
//   std::cout << A_a1 << std::endl;
//
//    // validation via invariant
//   std::cout << B_t1.cwiseProduct(B_a1).sum() << "=?="
//        << A_t1.cwiseProduct(A_a1).sum() << std::endl;
//}
//
//// a*B+C
//void aBpC() {
//    const int n = 3, m = 2;
//
//    // primal
//    ScalarValueType a = MatrixType::Random(1, 1)(0, 0);
//    MatrixType B = MatrixType::Random(n, m), C = MatrixType::Random(n, m), D;
//    D = a * B + C;
//   std::cout << D << std::endl;
//
//    // tangent
//    ScalarValueType a_t1 = MatrixType::Random(1, 1)(0, 0);
//    MatrixType B_t1 = MatrixType::Random(n, m), C_t1 = MatrixType::Random(n, m), D_t1;
//    D_t1 = a_t1 * B + a * B_t1 + C_t1;
//   std::cout << D_t1 << std::endl;
//
//    // adjoint
//    ScalarValueType a_a1 = 0;
//    MatrixType B_a1 = MatrixType::Zero(n, m), C_a1 = MatrixType::Zero(n, m), D_a1 = MatrixType::Random(n, m);
//    a_a1 += B.cwiseProduct(D_a1).sum();
//    B_a1 += a * D_a1;
//    C_a1 += D_a1;
//   std::cout << a_a1 << std::endl << B_a1 << std::endl << C_a1 << std::endl;
//
//    // validation via invariant
//   std::cout << D_t1.cwiseProduct(D_a1).sum() << "=?="
//        << a_t1 * a_a1 + B_t1.cwiseProduct(B_a1).sum()
//        + C_t1.cwiseProduct(C_a1).sum() << std::endl;
//}
//
//// a*B-C
//void aBmC() {
//    const int n = 3, m = 2;
//
//    // primal
//    ScalarValueType a = MatrixType::Random(1, 1)(0, 0);
//    MatrixType B = MatrixType::Random(n, m), C = MatrixType::Random(n, m), D;
//    D = a * B - C;
//   std::cout << D << std::endl;
//
//    // tangent
//    ScalarValueType a_t1 = MatrixType::Random(1, 1)(0, 0);
//    MatrixType B_t1 = MatrixType::Random(n, m), C_t1 = MatrixType::Random(n, m), D_t1;
//    D_t1 = a_t1 * B + a * B_t1 - C_t1;
//   std::cout << D_t1 << std::endl;
//
//    // adjoint
//    ScalarValueType a_a1 = 0;
//    MatrixType B_a1 = MatrixType::Zero(n, m), C_a1 = MatrixType::Zero(n, m), D_a1 = MatrixType::Random(n, m);
//    a_a1 += B.cwiseProduct(D_a1).sum();
//    B_a1 += a * D_a1;
//    C_a1 += -D_a1;
//   std::cout << a_a1 << std::endl << B_a1 << std::endl << C_a1 << std::endl;
//
//    // validation via invariant
//   std::cout << D_t1.cwiseProduct(D_a1).sum() << "=?="
//        << a_t1 * a_a1 + B_t1.cwiseProduct(B_a1).sum()
//        + C_t1.cwiseProduct(C_a1).sum() << std::endl;
//}
//
//// A*B
//void AB() {
//    const int n = 3, m = 2, p = 3;
//
//    // primal
//    MatrixType A = MatrixType::Random(n, m), B = MatrixType::Random(m, p), C;
//    C = A * B;
//   std::cout << C << std::endl;
//
//    // tangent
//    MatrixType A_t1 = MatrixType::Random(n, m), B_t1 = MatrixType::Random(m, p), C_t1;
//    C_t1 = A_t1 * B + A * B_t1;
//   std::cout << C_t1 << std::endl;
//
//    // adjoint
//    MatrixType A_a1 = MatrixType::Zero(n, m), B_a1 = MatrixType::Zero(m, p), C_a1 = MatrixType::Random(n, p);
//    A_a1 += C_a1 * B.transpose();
//    B_a1 += A.transpose() * C_a1;
//   std::cout << A_a1 << std::endl << B_a1 << std::endl;
//
//    // validation via invariant
//   std::cout << C_t1.cwiseProduct(C_a1).sum() << "=?="
//        << A_t1.cwiseProduct(A_a1).sum()
//        + B_t1.cwiseProduct(B_a1).sum() << std::endl;
//}
//
//// A*X=B by Cholesky Factorization
//void AXeB_LLT() {
//    const int n = 3, m = 5;
//    // primal
//    MatrixType A = MatrixType::Random(n, n), B = MatrixType::Random(n, m), X;
//    A = A.transpose() * A;
//    // IN -->
//    auto C = A.llt(); X = C.solve(B);
//    //X = A.llt().solve(B);
//    // <-- 
//    std::cout << X << std::endl;
//    // tangent
//    MatrixType A_t1 = MatrixType::Random(n, n), B_t1 = MatrixType::Random(n, m), X_t1;
//    // OUT -->
//    X_t1 = C.solve(B_t1) - C.solve(A_t1 * X);
//    // <-- 
//   std::cout << X_t1 << std::endl;
//    // adjoint
//    MatrixType A_a1 = MatrixType::Zero(n, n), B_a1 = MatrixType::Zero(n, m), X_a1 = MatrixType::Random(n, m);
//    // OUT -->
//    B_a1 += C.solve(X_a1);
//    A_a1 += -C.solve(X_a1 * X.transpose());
//    // <-- 
//   std::cout << A_a1 << std::endl << B_a1 << std::endl;
//    // validation via invariant
//   std::cout << X_t1.cwiseProduct(X_a1).sum() << "=?="
//        << A_t1.cwiseProduct(A_a1).sum()
//        + B_t1.cwiseProduct(B_a1).sum() << std::endl;
//}
//
///* TODO:
//   1. handle arbitrarily nested expressions over the elemental operations
//      shown above (add binary subtraction)
//   2. handle bracketing, e.g.  auto C=(A1+A2).llt(); X=C.solve(B1*(B2-B3));
//   3. enable reapplication to own output for higher derivatives
//   3. validate (differential invariant, finite differences)
//*/
//
