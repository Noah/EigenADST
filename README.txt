Automatic Differentiation Tool for Matrix Expressions of the Eigen library (Windows/Linux).
Eigen and vcpkg are linked to this project as a submodule, so cloning must happen with the --recursive flag.
( git clone https://git.rwth-aachen.de/Noah/EigenADCP.git --recursive )

Navigate to thirdParty/vcpkg and install vcpkg using the bootstrap script.
( LINUX ./bootstrap-vcpkg.sh )

Install wxWidgets and antlr4 using vcpkg.
( ./vcpkg install wxwidgets:<target_triplet> 
  WIN64 Bit ./vcpkg install wxwidgets:x64-windows
			./vcpkg install antrl4:x64-windows
  LINUX		./vcpkg install wxwidgets 
			./vcpkg install antlr4 )

Configure project using CMake presets with CMake.
( LINUX cmake --preset=linux-debug-native )

Navigate in public/build/&{presetName} and build the project using Make.
( LINUX make )
